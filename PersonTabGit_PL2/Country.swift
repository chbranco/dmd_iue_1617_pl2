//
//  Country.swift
//  PersonTabGit_PL2
//
//  Created by Luis Marcelino on 17/10/2016.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

struct Country {
    let name:String
    let population:Int
    let capital:String
}
