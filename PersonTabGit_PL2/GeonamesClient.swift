//
//  GeonamesClient.swift
//  PersonTabGit_PL2
//
//  Created by Luis Marcelino on 28/10/2016.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class GeonamesClient {
    
    // http://api.geonames.org/countryInfoJSON?formatted=true&lang=en&country=DE&username=desenvolvimentoswift&style=full
    static func fetchCountry (countryCode:String, completion: @escaping (Country?) -> Void) {
        if let url = URL(string:"http://api.geonames.org/countryInfoJSON?formatted=true&lang=en&country=\(countryCode)&username=desenvolvimentoswift&style=full") {
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if data == nil {
                    completion(nil)
                    return
                }
                if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any] {
                    if let jsonArray = jsonDic["geonames"] as? [[String:Any]] {
                        for jsonCountry in jsonArray {
                            let country = Country.parse(json: jsonCountry)
                            completion(country)
                            return
                        }
                    }
                }
                completion(nil)
            })
            dataTask.resume()
        }
        else {
            completion(nil)
        }
    }
    
}
