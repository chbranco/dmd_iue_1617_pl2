//
//  ArchivingRepository.swift
//  PersonTabGit_PL2
//
//  Created by Catarina Silva on 13/10/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class ArchivingRepository:RepositoryProtocol {
    
    //singleton repository
    static let repository = ArchivingRepository() //shared Instance
    fileprivate init () {}
    
    var people = [Person]()
    
    var documentsPath = URL(fileURLWithPath: "")
    var filePath = URL(fileURLWithPath: "")
    
    func savePeople() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0])
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("people.data", isDirectory: false)
        print(filePath)
        
        let path = filePath.path
        
        if NSKeyedArchiver.archiveRootObject(people, toFile: path) {
            print("Successfully saved people")
        } else {
            print("Failure saving people")
        }
    }
    
    func loadPeople() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0])
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("people.data", isDirectory: false)
        print(filePath)
        
        let path = filePath.path
        
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [Person]{
            people = newData
        }

    }
}
