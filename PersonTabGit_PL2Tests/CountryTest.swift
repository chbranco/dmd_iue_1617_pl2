//
//  CountryTest.swift
//  PersonTabGit_PL2
//
//  Created by Luis Marcelino on 17/10/2016.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import XCTest

class CountryTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCountryParse() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let responseString = "{\"geonames\":[{\"continent\":\"EU\",\"capital\":\"Berlino\",\"languages\":\"de\",\"geonameId\":2921044,\"south\":47.2700866656415,\"isoAlpha3\":\"DEU\",\"north\":55.05838360080724,\"fipsCode\":\"GM\",\"population\":\"81802257\",\"east\":15.041815651616307,\"isoNumeric\":\"276\",\"areaInSqKm\":\"357021.0\",\"countryCode\":\"DE\",\"west\":5.866250350725656,\"countryName\":\"Germania\",\"continentName\":\"Europa\",\"currencyCode\":\"EUR\"}]}"
        let responseData = responseString.data(using: .utf8)
        
        if let jsonDic = try? JSONSerialization.jsonObject(with: responseData!, options: .allowFragments) as! [String:Any] {

            if let jsonArray = jsonDic["geonames"] as? [[String:Any]] {
                for jsonCountry in jsonArray {
                    let country = Country.parse(json: jsonCountry)
                    XCTAssertEqual(country?.name, "Germania")
                    XCTAssertEqual(country?.capital, "Berlino")
                    XCTAssertEqual(country?.population, 81802257)
                }
            }

        }
        
    }
    
    
    func testCountryFetch()  {
        
        let ex = expectation(description: "Contact server")
        
        GeonamesClient.fetchCountry(countryCode: "PT") { (country) in
            
                XCTAssertEqual(country?.name, "Portugal")
                ex.fulfill()
        }
        
        waitForExpectations(timeout: 10) { (error) in
            if error != nil {
                XCTFail("No answer")
            }
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
